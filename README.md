[![gitlab pages deployed page link](https://img.shields.io/badge/Gitlab-Pages-orange?logo=gitlab)](https://nicolalandro.gitlab.io/flamingo-game/)

# Flamingo Game with Godot
This repo is a flamingo game godot project.

![Game image](docs/game.png)

# Serve Web

```
cd build/web
npx local-web-server --cors.embedder-policy "require-corp" --cors.opener-policy "same-origin" --directory .
```


# References
* godot
* blender
* nodejs and npx for serve web
* Fix coarse error: https://github.com/gzuidhof/coi-serviceworker explained also [here](https://stackoverflow.com/questions/68609682/is-there-any-way-to-use-sharedarraybuffer-on-github-pages)
