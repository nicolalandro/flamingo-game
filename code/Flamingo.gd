extends Node3D

var speed = 0.1
var rotate_speed = 1

var point = 0
var win_point = 5
var win = false

@onready var anim = $flamingo/AnimationPlayer
@onready var label_point = $"../2D view/PointPanel/Points"
@onready var win_panel = $"../2D view/WinPanel"
@onready var shrimp = $"../Shrimp"
@onready var restart_button = $"../2D view/Button"

func _ready():
	anim.speed_scale = 15

func on_press_restart():
	point = 0
	win = false
	win_panel.visible = false
	restart_button.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if win:
		anim.play("idle", -1, 0.5)
	else:
		if Input.is_action_pressed("up"):
			anim.play("walk 2")
			self.translate(Vector3.FORWARD * speed)
		if Input.is_action_pressed("down"):
			anim.play("walk 2")
			self.translate(-Vector3.FORWARD * speed)
		if Input.is_action_pressed("left"):
			self.rotation_degrees.y += rotate_speed
		if Input.is_action_pressed("right"):
			self.rotation_degrees.y -= rotate_speed




func _on_area_3d_area_entered(area):
	point = 1 + point
	label_point.text = str(point)
	shrimp.position = Vector3(randf()*10,0,randf()*10)
	if point == win_point:
		win = true
		win_panel.visible = true
		restart_button.visible = true
	# TODO move Shrimp

func _on_button_pressed():
	point = 0
	win = false
	win_panel.visible = false
	restart_button.visible = false
